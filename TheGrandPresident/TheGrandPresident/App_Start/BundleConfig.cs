﻿using System.Web;
using System.Web.Optimization;

namespace TheGrandPresident
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
            bundles.Add(new StyleBundle("~/Content/css/common").Include(
                "~/Content/bower_components/bootstrap/dist/css/bootstrap.min.css",
                 "~/Content/bower_components/font-awesome/css/font-awesome.min.css",
                 "~/Content/bower_components/Ionicons/css/ionicons.min.css",
                 "~/Content/dist/css/AdminLTE.min.css",
                 "~/Content/dist/css/skins/skin-blue.min.css"
                 //,"~/Content/dist/css/family_Source_sans_Pro.css"
                 ));

            bundles.Add(new ScriptBundle("~/bundles/js/common").Include(
                     "~/Content/bower_components/jquery/dist/jquery.min.js",
                     "~/Content/bower_components/bootstrap/dist/js/bootstrap.min.js",
                     "~/Content/dist/js/adminlte.min.js"));
        }
    }
}
