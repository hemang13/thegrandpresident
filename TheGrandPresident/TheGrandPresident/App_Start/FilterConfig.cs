﻿using System.Web;
using System.Web.Mvc;
using TheGrandPresident.Models.Common;
using System.Linq;
using TheGrandPresident.Models;
using System.Security.Claims;

namespace TheGrandPresident
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }

    public class SessionTimeoutAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;

            if (filterContext.HttpContext.Request.IsAuthenticated && HttpContext.Current.Session["UserProfile"] == null)
            {
                bool reject = true;
                var identity = HttpContext.Current.User.Identity as ClaimsIdentity;
                if (identity != null && identity.Claims.Where(c => c.Type == ClaimTypes.Sid).ToList().Count > 0)
                {
                    using (var dbContext = new TheGrandPresidentEntities())
                    {
                        string getUserId = identity.Claims.Where(c => c.Type == ClaimTypes.Sid).FirstOrDefault().Value;

                        Login loginInfo = dbContext.Logins.Where(e => e.UserId == getUserId).FirstOrDefault();

                        UserProfile userProfile = new UserProfile()
                        {
                            Id = loginInfo.Id,
                            LoginTypeId = loginInfo.LoginTypeId,
                            UserId = loginInfo.UserId,
                            Password = loginInfo.Password,
                            CreatedOn = loginInfo.CreatedOn,
                            CreatedBy = loginInfo.CreatedBy,
                            ModifiedOn = loginInfo.ModifiedOn,
                            ModifiedBy = loginInfo.ModifiedBy,
                            IsActive = loginInfo.IsActive
                        };
                        if (userProfile != null && userProfile.IsActive != false)
                        {
                            HttpContext.Current.Session["UserProfile"] = userProfile;
                            reject = false;
                        }
                    }
                }
                if (reject)
                {
                    //reject url
                    filterContext.Result = new RedirectResult("~/Account/SignOut");
                }

            }

            base.OnActionExecuting(filterContext);

            var session = filterContext.HttpContext.Session;

            if (session.IsNewSession || session["UserProfile"] == null)
            {
                if (filterContext.RequestContext.HttpContext.Request.IsAuthenticated)
                {
                    if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
                    {
                       // filterContext.Result = new JsonResult { Data = BusinessObjects.Constants.SessionExpiredJsonResult };
                    }
                    else
                    {
                        var authenticationManager = filterContext.HttpContext.GetOwinContext().Authentication;

                        if (authenticationManager != null)
                        {
                            authenticationManager.SignOut();

                           // Trace.TraceInformation(filterContext.HttpContext.User == null ? "User is null" : "User is not null");
                         //   Trace.TraceInformation(filterContext.HttpContext.User.Identity == null ? "User.Identity is null" : "User.Identity is not null");
                         //   Trace.TraceInformation(filterContext.RequestContext.HttpContext.Request.IsAuthenticated.ToString());
                        }

                        filterContext.Result = new ViewResult { ViewName = "~/Views/Shared/SessionExpired.cshtml" };
                    }
                }
            }
        }
    }
}
