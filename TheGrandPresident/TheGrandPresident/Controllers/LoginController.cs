﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TheGrandPresident.Models;
using TheGrandPresident.Models.Common;

namespace TheGrandPresident.Controllers
{
    [SessionTimeout]
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            Login loginModel = new Login();

            try
            {
                if (Session["UserProfile"] != null)
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception ex)
            {


            }
            //return RedirectToAction("Index1", "Login");
            return View(loginModel);
        }

        [HttpPost]
        public ActionResult Index(Login login)
         {
            try
            {
                TheGrandPresidentEntities db = new TheGrandPresidentEntities();
                Login loginModel = new Login();
                if (ModelState.IsValid)
                {
                    var IsValid = db.Logins.FirstOrDefault(e => e.UserId == login.UserId && e.Password == login.Password);
                    this.Session["UserProfile"] = IsValid;
                    if (IsValid != null)
                    {
                        if (IsValid.LoginTypeId == 1)
                            return RedirectToAction("BookingDetails", "HotelAdmin");
                        else if (IsValid.LoginTypeId == 2)
                            return RedirectToAction("BookingDetails", "HotelAdmin");
                        else if (IsValid.LoginTypeId == 3)
                            return RedirectToAction("Home", "Hotel");
                    }
                }
                return RedirectToAction("Index", "Login");
            }
            catch (Exception ex)
            {

                //throw;
            }
            return RedirectToAction("Index", "Login");

        }
        public ActionResult Home()
        {
            return View();
        }

        //[HttpPost]
        public ActionResult LogOut()
        {
            Session["UserProfile"] = null;
            Session.Clear();
            return RedirectToAction("Index", "Login");
        }

        


    }
}