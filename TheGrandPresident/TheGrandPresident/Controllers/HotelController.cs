﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TheGrandPresident.Models;
using TheGrandPresident.Models.Common;

namespace TheGrandPresident.Controllers
{
    public class HotelController : Controller
    {
        // GET: Hotel
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Home()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Home(Booking booking)
        {
            try
            {
                if (Session["UserProfile"] != null)
                {
                    if (booking != null)
                    {

                        //defalut
                        var adminObj = (TheGrandPresident.Models.Login)Session["UserProfile"];
                        int clientId = adminObj.ClientOrAdminId.Value;

                        //match otp
                        int lastOTP=0;
                        using (var dbContext = new TheGrandPresidentEntities())
                        {
                            SMSOTP lastSMSOTP = dbContext.SMSOTPs.Where(e => e.ClientId == clientId).OrderByDescending(e=>e.SmsId).FirstOrDefault();
                            if (lastSMSOTP != null)
                            {
                                lastOTP = lastSMSOTP.SmsOTP1;
                            }

                        }
                        if (booking.OTP == lastOTP)
                        {

                            //string aaa=CommonModel.SendSMS("9974850596", "12924600", "JAYDIP", "9979050382", "demo");

                            Booking newBooking = new Booking();
                            newBooking.FromDate = booking.FromDate;
                            newBooking.Todate = booking.Todate;
                            var a = booking.Todate.Value - booking.FromDate.Value;
                            int countBookingTotalDays = (int)a.Days;
                            newBooking.TotalDays = countBookingTotalDays;
                            newBooking.ClientId = clientId;//booking.ClientId;
                            newBooking.CustomerName = booking.CustomerName;
                            newBooking.CustomerContactNo = booking.CustomerContactNo;
                            newBooking.CouponId = booking.CouponId;
                            newBooking.StatusId = (int)BookingStatus.Pending;
                            newBooking.CreatedOn = booking.CreatedOn;
                            newBooking.CreatedBy = booking.CreatedBy;
                            newBooking.ModifiedOn = booking.ModifiedOn;
                            newBooking.ModifiedBy = booking.ModifiedBy;
                            newBooking.IsActive = true;
                            newBooking.CheckOut = false;
                            newBooking.Message = booking.Message;
                            newBooking.OTP = booking.OTP;

                            using (var dbContext = new TheGrandPresidentEntities())
                            {
                                //List<Coupon> activeCouponList = new List<Coupon>();
                                var activeCouponList = dbContext.Coupons.Where(e => e.IsUsed == false && e.ClientId == clientId).ToList().Take(countBookingTotalDays);
                                if (activeCouponList.Count() > 0 && activeCouponList.Count() >= countBookingTotalDays)
                                {
                                    var list = activeCouponList.Select(e => e.CouponId);
                                    List<Coupon> demo = dbContext.Coupons.Where(x => list.Contains(x.CouponId)).ToList();
                                    demo.ForEach(e => e.IsUsed = true);

                                    dbContext.Bookings.Add(newBooking);
                                    dbContext.SaveChanges();
                                    
                                }
                                else
                                {
                                    //coupon over error
                                }
                            }
                        }
                    }
                    return View();
                    //return RedirectToAction("Home", "Hotel");
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return RedirectToAction("Index", "Login");
        }
        public ActionResult History(BookingDetails booking)
        {
            List<BookingDetails> listBooking = new List<BookingDetails>();
            if (Session["UserProfile"] != null)
            {
                var adminObj = (TheGrandPresident.Models.Login)Session["UserProfile"];
                int clientId = adminObj.ClientOrAdminId.Value;
            
                using (var dbContext = new TheGrandPresidentEntities())
                {
                    List<Booking> bookingList = new List<Booking>();
                    bookingList = dbContext.Bookings.Where(e => e.ClientId == /*booking.ClientId*/clientId).ToList();

                    listBooking = bookingList.Select(e => new BookingDetails()
                    {
                        BookingId = e.BookingId,
                        FromDate = e.FromDate,
                        Todate = e.Todate,
                        TotalDays = e.TotalDays,
                        ClientId = e.ClientId,
                        CustomerName = e.CustomerName,
                        CustomerContactNo = e.CustomerContactNo,
                        CouponId = e.CouponId,
                        StatusId = e.StatusId,
                        CreatedOn = e.CreatedOn,
                        CreatedBy = e.CreatedBy,
                        ModifiedOn = e.ModifiedOn,
                        ModifiedBy = e.ModifiedBy,
                        IsActive = e.IsActive,
                        ClientName = dbContext.Clients.Where(k => k.ClientId == e.ClientId).FirstOrDefault().ClientName,
                        StatusName = dbContext.Status.Where(y => y.StatusId == e.StatusId).FirstOrDefault().StatusName,
                        CheckInTimeList = dbContext.CheckInTimes.Where(h => h.IsActive == true).ToList()
                    }).ToList();
                }

                return View(listBooking);
            }
            return RedirectToAction("Index", "Login");
        }

        public ActionResult Coupon(CouponClientStatus obj)
        {
            if (Session["UserProfile"] != null)
            {
                var adminObj = (TheGrandPresident.Models.Login)Session["UserProfile"];
                int clientId = adminObj.ClientOrAdminId.Value;
                //CouponClientStatus obj = new CouponClientStatus();
                if (obj != null)
                {
                    using (var dbContext = new TheGrandPresidentEntities())
                    {
                        obj.TotalCoupon = dbContext.Coupons.Where(e => e.ClientId == /*booking.ClientId*/clientId).Count();
                        obj.UsedCoupon = dbContext.Coupons.Where(e => e.IsUsed == true && e.ClientId == /*booking.ClientId*/clientId).Count();
                        obj.RemainCoupon = obj.TotalCoupon - obj.UsedCoupon;

                    }
                }
                return View(obj);
            }
            return RedirectToAction("Index", "Login");
        }

        //public RandomNumberResponse GenerateRandomNumber()
        [HttpPost]
        public JsonResult GenerateRandomNumber()
        {
            var response = new RandomNumberResponse();
            try
            {
                if (Session["UserProfile"] != null)
                {
                    var adminObj = (TheGrandPresident.Models.Login)Session["UserProfile"];
                    int clientId = adminObj.ClientOrAdminId.Value;


                    response.IsSuccess = false;
                    //auto gerated
                    //string randomNumber = CommonModel.getRandomDigit(5);
                    //setting
                    string randomNumber = "12345";

                    Models.SMSOTP smsOTP = new Models.SMSOTP();
                    smsOTP.ClientId = clientId;
                    smsOTP.SmsOTP1 = Convert.ToInt32(randomNumber);
                    smsOTP.CreatedOn = DateTime.Now;
                    using (var dbContext = new TheGrandPresidentEntities())
                    {
                        dbContext.SMSOTPs.Add(smsOTP);
                        dbContext.SaveChanges();
                    }
                    if (randomNumber != "")
                    {
                        response.IsSuccess = true;
                        response.RandomNumber = randomNumber;
                        //string aaa = CommonModel.SendSMS("9974850596", "12924600", "JAYDIP", "9979050382", randomNumber);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            //return response;
            return Json(response);
        }

    }
}