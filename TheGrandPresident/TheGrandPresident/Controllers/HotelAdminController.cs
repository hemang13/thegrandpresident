﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TheGrandPresident.Models;
using TheGrandPresident.Models.Common;

namespace TheGrandPresident.Controllers
{
    public class HotelAdminController : Controller
    {
        // GET: HotelAdmin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult BookingDetails()
        {
            List<BookingDetails> listBookingDetails = new List<BookingDetails>();

            if (Session["UserProfile"] != null)
            {
                using (var dbContext = new TheGrandPresidentEntities())
                {
                    List<Booking> getALLBooking = new List<Booking>();
                    getALLBooking = dbContext.Bookings.ToList();

                    listBookingDetails = getALLBooking.Select(e => new BookingDetails()
                    {
                        BookingId = e.BookingId,
                        FromDate = e.FromDate,
                        Todate = e.Todate,
                        TotalDays = e.TotalDays,
                        ClientId = e.ClientId,
                        CustomerName = e.CustomerName,
                        CustomerContactNo = e.CustomerContactNo,
                        CouponId = e.CouponId,
                        StatusId = e.StatusId,
                        CreatedOn = e.CreatedOn,
                        CreatedBy = e.CreatedBy,
                        ModifiedOn = e.ModifiedOn,
                        ModifiedBy = e.ModifiedBy,
                        IsActive = e.IsActive,
                        CheckOut = e.CheckOut,
                        BillNumber=e.BillNumber,
                        TotalAmount=e.TotalAmount,
                        Message =e.Message,
                        ClientName = dbContext.Clients.Where(k => k.ClientId == e.ClientId).FirstOrDefault().ClientName,
                        StatusName = dbContext.Status.Where(y => y.StatusId == e.StatusId).FirstOrDefault().StatusName
                    }).ToList();
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View(listBookingDetails);

        }

        public ActionResult PendingBooking()
        {
            List<BookingDetails> listBookingDetails = new List<BookingDetails>();
            if (Session["UserProfile"] != null)
            {
                var adminObj = (TheGrandPresident.Models.Login)Session["UserProfile"];
                if (adminObj.LoginTypeId != (int)LoginTypeStatus.HotelEmployee)
                {
                    using (var dbContext = new TheGrandPresidentEntities())
                    {
                        List<Booking> getALLBooking = new List<Booking>();
                        getALLBooking = dbContext.Bookings.Where(e => e.StatusId == (int)BookingStatus.Pending).ToList();

                        listBookingDetails = getALLBooking.Select(e => new BookingDetails()
                        {
                            BookingId = e.BookingId,
                            FromDate = e.FromDate,
                            Todate = e.Todate,
                            TotalDays = e.TotalDays,
                            ClientId = e.ClientId,
                            CustomerName = e.CustomerName,
                            CustomerContactNo = e.CustomerContactNo,
                            CouponId = e.CouponId,
                            StatusId = e.StatusId,
                            CreatedOn = e.CreatedOn,
                            CreatedBy = e.CreatedBy,
                            ModifiedOn = e.ModifiedOn,
                            ModifiedBy = e.ModifiedBy,
                            IsActive = e.IsActive,
                            ClientName = dbContext.Clients.Where(k => k.ClientId == e.ClientId).FirstOrDefault().ClientName,
                            StatusName = dbContext.Status.Where(y => y.StatusId == e.StatusId).FirstOrDefault().StatusName
                        }).ToList();
                    }
                }
                else
                {
                    return RedirectToAction("Logout", "Login");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View(listBookingDetails);
        }

        public ActionResult CancelBooking()
        {
            List<BookingDetails> listBookingDetails = new List<BookingDetails>();
            if (Session["UserProfile"] != null)
            {
                var adminObj = (TheGrandPresident.Models.Login)Session["UserProfile"];
                if (adminObj.LoginTypeId != (int)LoginTypeStatus.HotelEmployee)
                {
                    using (var dbContext = new TheGrandPresidentEntities())
                    {
                        List<Booking> getALLBooking = new List<Booking>();
                        getALLBooking = dbContext.Bookings.Where(e => e.StatusId == (int)BookingStatus.Canceled).ToList();

                        listBookingDetails = getALLBooking.Select(e => new BookingDetails()
                        {
                            BookingId = e.BookingId,
                            FromDate = e.FromDate,
                            Todate = e.Todate,
                            TotalDays = e.TotalDays,
                            ClientId = e.ClientId,
                            CustomerName = e.CustomerName,
                            CustomerContactNo = e.CustomerContactNo,
                            CouponId = e.CouponId,
                            StatusId = e.StatusId,
                            CreatedOn = e.CreatedOn,
                            CreatedBy = e.CreatedBy,
                            ModifiedOn = e.ModifiedOn,
                            ModifiedBy = e.ModifiedBy,
                            IsActive = e.IsActive,
                            ClientName = dbContext.Clients.Where(k => k.ClientId == e.ClientId).FirstOrDefault().ClientName,
                            StatusName = dbContext.Status.Where(y => y.StatusId == e.StatusId).FirstOrDefault().StatusName
                        }).ToList();
                    }
                }
                else
                {
                    return RedirectToAction("Logout", "Login");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View(listBookingDetails);

        }

        public ActionResult Booked()
        {

            List<BookingDetails> listBookingDetails = new List<BookingDetails>();
            if (Session["UserProfile"] != null)
            {
                var adminObj = (TheGrandPresident.Models.Login)Session["UserProfile"];
                if (adminObj.LoginTypeId != (int)LoginTypeStatus.HotelEmployee)
                {
                    using (var dbContext = new TheGrandPresidentEntities())
                    {
                        List<Booking> getALLBooking = new List<Booking>();
                        getALLBooking = dbContext.Bookings.Where(e => e.StatusId == (int)BookingStatus.Booked).ToList();

                        listBookingDetails = getALLBooking.Select(e => new BookingDetails()
                        {
                            BookingId = e.BookingId,
                            FromDate = e.FromDate,
                            Todate = e.Todate,
                            TotalDays = e.TotalDays,
                            ClientId = e.ClientId,
                            CustomerName = e.CustomerName,
                            CustomerContactNo = e.CustomerContactNo,
                            CouponId = e.CouponId,
                            StatusId = e.StatusId,
                            CreatedOn = e.CreatedOn,
                            CreatedBy = e.CreatedBy,
                            ModifiedOn = e.ModifiedOn,
                            ModifiedBy = e.ModifiedBy,
                            IsActive = e.IsActive,
                            ClientName = dbContext.Clients.Where(k => k.ClientId == e.ClientId).FirstOrDefault().ClientName,
                            StatusName = dbContext.Status.Where(y => y.StatusId == e.StatusId).FirstOrDefault().StatusName
                        }).ToList();
                    }
                }
                else
                {
                    return RedirectToAction("Logout", "Login");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View(listBookingDetails);
        }

        public ActionResult ClientList()
        {
            List<ClientDetails> listClientDetails = new List<ClientDetails>();
            if (Session["UserProfile"] != null)
            {
                var adminObj = (TheGrandPresident.Models.Login)Session["UserProfile"];
                if (adminObj.LoginTypeId != (int)LoginTypeStatus.HotelEmployee)
                {
                    using (var dbContext = new TheGrandPresidentEntities())
                    {
                        List<Client> getALLClient = new List<Client>();
                        getALLClient = dbContext.Clients.ToList();

                        listClientDetails = getALLClient.Select(e => new ClientDetails()
                        {
                            ClientId = e.ClientId,
                            ClientName = e.ClientName,
                            CreatedOn = e.CreatedOn,
                            CreatedBy = e.CreatedBy,
                            ModifiedOn = e.ModifiedOn,
                            ModifiedBy = e.ModifiedBy,
                            IsActive = e.IsActive,
                            TotalCoupon = dbContext.Coupons.Where(x => x.ClientId == e.ClientId).Count(),
                            UsedCoupon = dbContext.Coupons.Where(x => x.IsUsed == true && x.ClientId == e.ClientId).Count(),
                            //RemainCoupon = TotalCoupon - RemainCoupon,

                        }).ToList();
                    }
                }
                else
                {
                    return RedirectToAction("Logout", "Login");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View(listClientDetails);
        }

        public ActionResult Coupon()
        {
            List<CouponDetails> listCouponDetails = new List<CouponDetails>();
            if (Session["UserProfile"] != null)
            {
                var adminObj = (TheGrandPresident.Models.Login)Session["UserProfile"];
                if (adminObj.LoginTypeId != (int)LoginTypeStatus.HotelEmployee)
                {
                    using (var dbContext = new TheGrandPresidentEntities())
                    {
                        List<Coupon> getALLCoupon = new List<Coupon>();
                        getALLCoupon = dbContext.Coupons.ToList();

                        listCouponDetails = getALLCoupon.Select(e => new CouponDetails()
                        {
                            CouponId = e.CouponId,
                            CouponCode = e.CouponCode,
                            ClientId = e.ClientId,
                            ExpireDate = e.ExpireDate.Value,
                            IsUsed = e.IsUsed,
                            ClientName = dbContext.Clients.Where(x => x.ClientId == e.ClientId).FirstOrDefault().ClientName

                        }).ToList();
                    }
                }
                else
                {
                    return RedirectToAction("Logout", "Login");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View(listCouponDetails);
        }

        [HttpPost]
        public ActionResult CancelBooking(int id)
        {
            if (Session["UserProfile"] != null)
            {
                using (var dbContext = new TheGrandPresidentEntities())
                {
                    Booking getBookingData = dbContext.Bookings.Where(e => e.BookingId == id).FirstOrDefault();
                    if (getBookingData != null)
                    {
                        var a = getBookingData.Todate.Value - getBookingData.FromDate.Value;
                        int countBookingTotalDays = (int)a.Days;

                        var changeCoupounStatus = dbContext.Coupons.Where(e => e.ClientId == getBookingData.ClientId && e.IsUsed == true).OrderByDescending(e => e.CouponId).Take(countBookingTotalDays);
                        var list = changeCoupounStatus.Select(e => e.CouponId);
                        List<Coupon> demo = dbContext.Coupons.Where(x => list.Contains(x.CouponId)).ToList();
                        demo.ForEach(e => e.IsUsed = false);

                        getBookingData.StatusId = (int)BookingStatus.Canceled;
                        dbContext.SaveChanges();
                    }
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return Json(true);
        }

        [HttpPost]
        public JsonResult ApproveBooking(int id)
        {
            if (Session["UserProfile"] != null)
            {
                var adminObj = (TheGrandPresident.Models.Login)Session["UserProfile"];
                
                using (var dbContext = new TheGrandPresidentEntities())
                {
                    Booking getBookingData = dbContext.Bookings.Where(e => e.BookingId == id).FirstOrDefault();
                    if (getBookingData != null)
                    {
                        //var a = getBookingData.Todate.Value - getBookingData.FromDate.Value;
                        //int countBookingTotalDays = (int)a.Days;

                        //var changeCoupounStatus = dbContext.Coupons.Where(e => e.ClientId == getBookingData.ClientId && e.IsUsed == true).OrderByDescending(e => e.CouponId).Take(countBookingTotalDays);
                        //var list = changeCoupounStatus.Select(e => e.CouponId);
                        //List<Coupon> demo = dbContext.Coupons.Where(x => list.Contains(x.CouponId)).ToList();
                        //demo.ForEach(e => e.IsUsed = false);

                        getBookingData.StatusId = (int)BookingStatus.Booked;
                        getBookingData.ModifiedBy = adminObj.UserId;
                        getBookingData.ModifiedOn = DateTime.Now;
                        dbContext.SaveChanges();
                    }
                }
            }

            return Json(true);
        }

        [HttpPost]
        public JsonResult CheckOutBooking(int id, string TotalAmount, string BillNumber)
        {
            try
            {
                if (Session["UserProfile"] != null)
                {
                    var adminObj = (TheGrandPresident.Models.Login)Session["UserProfile"];

                    using (var dbContext = new TheGrandPresidentEntities())
                    {
                        Booking getBookingData = dbContext.Bookings.Where(e => e.BookingId == id).FirstOrDefault();
                        if (getBookingData != null)
                        {
                            //var a = getBookingData.Todate.Value - getBookingData.FromDate.Value;
                            //int countBookingTotalDays = (int)a.Days;

                            //var changeCoupounStatus = dbContext.Coupons.Where(e => e.ClientId == getBookingData.ClientId && e.IsUsed == true).OrderByDescending(e => e.CouponId).Take(countBookingTotalDays);
                            //var list = changeCoupounStatus.Select(e => e.CouponId);
                            //List<Coupon> demo = dbContext.Coupons.Where(x => list.Contains(x.CouponId)).ToList();
                            //demo.ForEach(e => e.IsUsed = false);

                            getBookingData.CheckOut = true;
                            getBookingData.TotalAmount = TotalAmount;
                            getBookingData.BillNumber = BillNumber;
                            getBookingData.ModifiedBy = adminObj.UserId;
                            getBookingData.ModifiedOn = DateTime.Now;
                            dbContext.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            
            return Json(true);
        }
    }
}