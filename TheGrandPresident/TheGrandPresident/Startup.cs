﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TheGrandPresident.Startup))]
namespace TheGrandPresident
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
