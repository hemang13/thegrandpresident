﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheGrandPresident.Models.Common
{
    public class Enum
    {
    }

    public enum BookingStatus
    {
        Booked=1,
        Canceled=2,
        Pending=3
    }

    public enum LoginTypeStatus
    {
        HotelAdmin = 1,
        HotelEmployee = 2,
        Client = 3
    }
}