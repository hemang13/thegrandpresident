﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.IO;
using System.Collections.Concurrent;

namespace TheGrandPresident.Models.Common
{
    public class CommonModel
    {
        public static UserProfile CurrentUser
        {
            get
            {
                UserProfile userProfile = null;
                if (HttpContext.Current.Request.IsAuthenticated && HttpContext.Current.Session["UserProfile"] != null)
                {
                    userProfile = (UserProfile)HttpContext.Current.Session["UserProfile"];
                }
                return userProfile;
            }
        }

        public static string SendSMS(string userid,string password,string senderid,string to_number,string your_message)
        {
            //string concateStr=$"Hello {userid}. Today is {password}.";
            string concateString= $"http://dnd.textbox.in/smsstatuswithid.aspx?mobile={userid}&pass={password}&senderid={senderid}&to={to_number}&msg={your_message}";
            Uri targetUri = new Uri(concateString);
            HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(targetUri);
            webRequest.Method = WebRequestMethods.Http.Get;
            try
            {
                string webResponse = string.Empty;
                using (HttpWebResponse getresponse = (HttpWebResponse)webRequest.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(getresponse.GetResponseStream()))
                    {
                        webResponse = reader.ReadToEnd();
                        reader.Close();
                    }
                    getresponse.Close();
                }
                return webResponse;
            }
            catch (System.Net.WebException ex)
            {
                return "Request-Timeout";
            }
            catch (Exception ex)
            {
                return "error";
            }
            finally
            {
                webRequest.Abort();
            }
        }

        static Random random = new Random();
        public static int getRandomFor1To5()
        {
            try
            {
                return random.Next(1, 5);
            }
            catch (Exception)
            {
                return 1;
            }
        }

        public static string getRandomDigit(int Leanth)
        {
            char[] chars = "1234567890".ToCharArray();

            string password = string.Empty;


            for (int i = 0; i < Leanth; i++)
            {
                int x = random.Next(1, chars.Length);
                //Don't Allow Repetation of Characters
                if (!password.Contains(chars.GetValue(x).ToString()))
                    password += chars.GetValue(x);
                else
                    i--;
            }
            return password;
        }
    }
}