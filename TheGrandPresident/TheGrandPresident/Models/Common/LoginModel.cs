﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using TheGrandPresident.Models.Common;


namespace TheGrandPresident.Models.Common
{
    public class BaseModel
    {
        public BaseModel()
        {
            this.UserProfile = CommonModel.CurrentUser;
        }
        public UserProfile UserProfile;
        public List<FunctionAction> MultipleActions;
        public string AccessToken { get; set; }
    }
    
}