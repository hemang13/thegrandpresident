﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheGrandPresident.Models.Common
{
    public class Shared
    {

    }
    public class UserProfile
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public Nullable<int> LoginTypeId { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }

    public class CouponClientStatus
    {
        public int TotalCoupon { get; set; }
        public int UsedCoupon { get; set; }
        public int RemainCoupon { get; set; }
    }

    public class BookingDetails
    {
        public int BookingId { get; set; }
        public Nullable<System.DateTime> FromDate { get; set; }
        public Nullable<System.DateTime> Todate { get; set; }
        public Nullable<int> TotalDays { get; set; }
        public Nullable<int> ClientId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerContactNo { get; set; }
        public Nullable<int> CouponId { get; set; }
        public Nullable<int> StatusId { get; set; }
        public Nullable<bool> CheckOut { get; set; }
        public string BillNumber { get; set; }
        public string TotalAmount { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public string ClientName { get; set; }
        public string StatusName { get; set; }
        public string Message { get; set; }
        public int OTP { get; set; }

        public List<CheckInTime> CheckInTimeList { get; set; }
    }

    public class ClientDetails
    {
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public int TotalCoupon { get; set; }
        public int RemainCoupon { get; set; }
        public int UsedCoupon { get; set; }

    }

    public class CouponDetails
    {
        public int CouponId { get; set; }
        public string CouponCode { get; set; }
        public Nullable<int> ClientId { get; set; }
        public Nullable<System.DateTime> ExpireDate { get; set; }
        public Nullable<bool> IsUsed { get; set; }

        public string ClientName { get; set; }

    }

    public class RandomNumberResponse
    {
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
        public string RandomNumber { get; set; }
    }

    //public class SMSOTP
    //{
    //    public int SmsId { get; set; }
    //    public Nullable<int> SmsOTP1 { get; set; }
    //    public Nullable<int> ClientId { get; set; }
    //}

}