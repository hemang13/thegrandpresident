﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net.Http;

namespace TheGrandPresident.Models.Common
{
    public class Helper
    {

    }
    public class FunctionAction
    {
        public string ActionName { get; set; }
        public string FunctionName { get; set; }
        public string IconClass { get; set; }
        public string TechnicalName { get; set; }
    }
}